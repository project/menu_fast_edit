# Menu Fast Edit

The Menu Fast Edit module exposes the "Title" and "URL" fields for menu links directly on the menu configuration page.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/menu_fast_edit).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/menu_fast_edit).

## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers
- Supporting organizations

## Requirements

This module requires no modules outside of Drupal core.

## Installation
Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

The module has no menu or modifiable settings.

## Maintainers

- Steven Wright - [stevenc](https://www.drupal.org/u/stevenc)
- Rick Hawkins - [rlhawk](https://www.drupal.org/u/rlhawk)
- Tor F. Jacobsen - [torfj](https://www.drupal.org/u/torfj)

## Supporting organizations

- [Slalom](https://www.drupal.org/slalom)
- [University of Washington](https://www.drupal.org/university-of-washington)
